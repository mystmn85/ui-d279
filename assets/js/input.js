function getQuery() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });
    let value = params.emailAddress;
    return value;
}

function autofillInput(htmlInputEmail, query) {
    htmlInputEmail.value = query
}
